﻿using System.Linq;
using HandlebarsDotNet;

namespace Shopspray.PunchOut.Core.Templating
{
    class TruncateHelper : IHandlebarsHelper
    {
        public string Name => "Truncate";

        public string Prefix => "trunc";

        public string Definition => "Truncate a string to x chars. Usage: trunc Variable [chars]";

        public HandlebarsHelper Execute()
        {
            return (writer, context, parameters) =>
            {
                var inputValue = parameters.FirstOrDefault() as string;
                if (inputValue != null)
                {
                    var lenArg = parameters.Skip(1).FirstOrDefault() as string;
                    if (lenArg != null)
                    {
                        if (int.TryParse(lenArg, out int len) && len > 0)
                        {
                            if (inputValue.Length > len)
                            {
                                inputValue = inputValue.Substring(0, len);
                            }
                        }
                    }
                }
                writer.WriteSafeString(inputValue);
            };
        }
    }
}
