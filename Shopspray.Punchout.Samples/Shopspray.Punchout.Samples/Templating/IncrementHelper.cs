﻿using System.Linq;
using HandlebarsDotNet;

namespace Shopspray.PunchOut.Core.Templating
{
    class IncrementHelper : IHandlebarsHelper
    {
        public string Name => "Increment";

        public string Prefix => "inc";

        public string Definition => "Increment a variable. Usage \"inc @variable\" alternatively \"inc @variable 10\" to increment by 10.";

        public HandlebarsHelper Execute()
        {
            return (writer, context, parameters) =>
            {
                var valueObject = parameters.FirstOrDefault();
                var incObject = parameters.Skip(1).FirstOrDefault();
                var incrementBy = 1;
                if (incObject is int inc)
                    incrementBy = inc;
                if (valueObject is int value)
                {
                    writer.WriteSafeString((value + incrementBy).ToString());
                }
            };
        }
    }
}
