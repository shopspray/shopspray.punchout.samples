﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopspray.Punchout.Samples.Implementations
{
    public class SessionContextService
    {
        public T Get<T>(string itemName)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                HttpContext.Current.Session[itemName] != null)
            {
                return (T)HttpContext.Current.Session[itemName];
            }
            return default(T);
        }

        public void Set<T>(string itemName, T value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session[itemName] = value;
            }
        }

        public void Remove(string itemName)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session[itemName] = null;
            }
        }
    }
}