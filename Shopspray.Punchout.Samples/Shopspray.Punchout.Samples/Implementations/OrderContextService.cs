﻿using Shopspray.PunchOut.Core;
using Shopspray.PunchOut.Core.External;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Commerce.Order;
using Shopspray.PunchOut.Core.Models;

namespace Shopspray.Punchout.Samples.Implementations
{
    public class OrderContextService : IOrderContextService
    {
        private readonly ICartService _cartService;
        private readonly IOrderGroupCalculator _orderGroupCalculator;

        public OrderContextService(
            IOrderGroupCalculator orderGroupCalculator,
            ICartService cartService)
        {
            _cartService = cartService;
            _orderGroupCalculator = orderGroupCalculator;
        }


        public IReturnData GetEmptyOrderInfo()
        {
            return new ReturnDataImpl();
        }

        public IReturnData GetOrderInfo()
        {
            var cart = _cartService.CurrentCart;
            var returnData = CreateReturnData(cart);
            return returnData;
        }

        public IReturnData GetOrderInfo(IEnumerable<ProductDetailsRequest> products)
        {
            var items = new List<ItemDataImpl>();
            foreach (var pdr in products)
            {
                items.Add(new ItemDataImpl
                {
                    ProductId = pdr.ProductId,
                    Quantity = pdr.Quantity,
                    Currency = "SEK",
                    Description = "Lorem ipsum dolor sit amit.",
                    MatNr = "101047",
                    PriceUnit = 1,
                    Unit = "ST",
                    UnitPrice = 100m
                });
            }

            var data = new ReturnDataImpl
            {
                Id = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                CustomOrderData = new Dictionary<string, string>(),
                LineItems = items
            };

            return data;
        }


        public IReturnData Search(string searchString)
        {
            return new ReturnDataImpl();
        }

        private ReturnDataImpl CreateReturnData(ICart cart)
        {
            var returnData = new ReturnDataImpl
            {
                Id = Guid.NewGuid(),
                Created = DateTime.UtcNow,
                LineItems = MapItems(cart),
                OrderData = new OrderDataImpl()
                {
                    TotalPrice = cart.GetTotal(_orderGroupCalculator).Amount,
                    Currency = cart.Currency
                }
            };

            return returnData;
        }

        private IReadOnlyCollection<IItemData> MapItems(ICart cart)
        {
            return cart.GetAllLineItems()
                .Select(g => MapItem(g, cart) )
                .ToList();
        }

        private IItemData MapItem(ILineItem li, ICart cart)
        {
            var item = new ItemDataImpl
            {
                MatNr = li.Code,
                Description = li.DisplayName,
                LongDescription = li.DisplayName,
                ExtProductId = li.Code,
                VendorMatNr = li.Code,
                Quantity = li.Quantity,
                Unit = "ST",
                UnitPrice = li.PlacedPrice,
                Currency = cart.Currency,
                PriceUnit = 1,
                LeadTimeDays = 1,
                MatGroup = "46180000",
                ManufacturerCode = null,
                ManufacturerMatNr = null,
                Vendor = null,
                Contract = null,
                ContractItem = null,
                CustField1 = null,
                CustField2 = null,
                CustField3 = null,
                CustField4 = null,
                CustField5 = null,
                CustomItemData = new Dictionary<string, string>(0),
                IsService = false
            };
            return item;
        }

        private static string FormatPrice(decimal priceValue)
        {
            return priceValue.ToString("0.000", CultureInfo.InvariantCulture);
        }

        private class OrderDataImpl : IOrderData
        {
            public decimal TotalPrice { get; set; }
            public string Currency { get; set; }
        }

        private class ReturnDataImpl : IReturnData
        {
            public Guid Id { get; set; }
            public DateTime Created { get; set; }
            public IReadOnlyCollection<IItemData> LineItems { get; set; }
            public IReadOnlyDictionary<string, string> CustomOrderData { get; set; }
            public IOrderData OrderData { get; set; }
        }

        private class ItemDataImpl : IItemData
        {
            public string ProductId { get; set; }
            public string Description { get; set; }
            public string MatNr { get; set; }
            public decimal Quantity { get; set; }
            public string Unit { get; set; }
            public decimal UnitPrice { get; set; }
            public string Currency { get; set; }
            public int PriceUnit { get; set; }
            public int LeadTimeDays { get; set; }
            public string LongDescription { get; set; }
            public string Vendor { get; set; }
            public string VendorMatNr { get; set; }
            public string ManufacturerCode { get; set; }
            public string ManufacturerMatNr { get; set; }
            public string MatGroup { get; set; }
            public bool IsService { get; set; }
            public string Contract { get; set; }
            public string ContractItem { get; set; }
            public string ExtProductId { get; set; }
            public string CustField1 { get; set; }
            public string CustField2 { get; set; }
            public string CustField3 { get; set; }
            public string CustField4 { get; set; }
            public string CustField5 { get; set; }
            public IReadOnlyDictionary<string, string> CustomItemData { get; set; }
        }
    }
}