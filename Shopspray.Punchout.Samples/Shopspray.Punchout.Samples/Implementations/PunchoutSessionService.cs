﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Shopspray.PunchOut.Core.Business;
using Microsoft.Owin.Security;

namespace Shopspray.Punchout.Samples.Implementations
{
    public class PunchoutSessionService
    {
        private SessionContextService SessionContextService { set; get; }
        private IAuthenticationManager _authenticationManager;

        private const string PunchOutSessionActiveCookieName = "posesac";
        private const string PoSettingsSessionConst = "POSessionSettings";

        public PunchoutSessionService(SessionContextService sessionContextService, IAuthenticationManager authenticationManager)
        {
            SessionContextService = sessionContextService;
            _authenticationManager = authenticationManager;
        }

        private static string PunchOutSessionActive
        {
            get
            {
                return CookieService.Get(PunchOutSessionActiveCookieName);
            }

            set
            {
                if (value == null)
                {
                    CookieService.Set(PunchOutSessionActiveCookieName, null, DateTime.Now.AddDays(-1));
                }
                else
                {
                    CookieService.Set(PunchOutSessionActiveCookieName, value);
                }
            }
        }

        private static void RemoveCookie()
        {
            PunchOutSessionActive = null;
        }

        private void RemoveSession()
        {
            SessionContextService.Remove(PoSettingsSessionConst);
        }

        public static bool IsCookieActive()
        {
            return PunchOutSessionActive != null;
        }

        public void StartSession(ISessionContext context)
        {
            PunchOutSessionActive = "T";
            SessionContextService.Set(PoSettingsSessionConst, context);
        }

        public ISessionContext GetSettings()
        {
            if (!IsCookieActive())
            {
                return null;
            }

            var session = SessionContextService.Get<ISessionContext>(PoSettingsSessionConst);

            if (session == null)
            {
                DestroySession();
            }

            return session;
        }

        public bool IsActive()
        {
            return IsCookieActive() && GetSettings() != null;
        }

        public void EndSession()
        {
            RemoveCookie();
            RemoveSession();
        }

        private void DestroySession()
        {
            //If session is cleared, so should the cookie be. And also logout. Illegal state.
            //PunchoutSessionHandler.Logout(); //Will invoke RemoveSession & RemoveCookie
            RemoveSession();
            RemoveCookie();

            //Not the best but this is what Core Project currently is doing
            //We could not use IUserManager (Internal) or _customerDatabase (circular reference).
            HttpContext.Current.Session.Abandon();
            _authenticationManager.SignOut();

            // If the state is illegal stop page processing and redirect to root page
            HttpContext.Current.Response.Redirect(VirtualPathUtility.ToAppRelative("~/"), true);
        }
    }
}