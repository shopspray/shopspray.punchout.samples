﻿using EPiServer.Commerce.Order;
using EPiServer.Reference.Commerce.Site.Features.AddressBook.Services;
using EPiServer.Reference.Commerce.Site.Features.Cart.Services;
using EPiServer.Reference.Commerce.Site.Features.Shared.Models;
using EPiServer.ServiceLocation;
using Mediachase.Commerce.Orders;
using Shopspray.PunchOut.Core;
using Shopspray.PunchOut.Core.Connect.Models.CXml.Address;
using Shopspray.PunchOut.Core.Connect.Models.CXml.Item;
using Shopspray.PunchOut.Core.Connect.Models.CXml.Request;
using Shopspray.PunchOut.Core.External;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPiServer.Reference.Commerce.Site.Implementations
{
    public class OrderRequestService : IOrderRequestService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ICartService _cartService;
        private readonly IAddressBookService _addressBookService;
        private readonly PunchoutSessionService _punchoutSessionService;
        public OrderRequestService(IOrderRepository orderRepository, ICartService cartService, IAddressBookService addressBookService, PunchoutSessionService punchoutSessionService)
        {
            _orderRepository = orderRepository;
            _cartService = cartService;
            _addressBookService = addressBookService;
            _punchoutSessionService = punchoutSessionService;
        }
        private void SetCartProperties(ICart cart, CXmlRequestDocument request)
        {
            cart.Properties.Add("PayloadId", request.PayloadId);
            var header = request.Request.OrderRequest.OrderRequestHeader;
            cart.Currency = header.Total.Money.Currency;
            cart.PricesIncludeTax = false;

        }
        private void AddCartItems(ICart cart, cXMLItemOut[] items)
        {
            foreach (var item in items)
            {
                _cartService.AddToCart(cart, item.ItemId.SupplierPartId, item.Quantity);
            }
        }

        private IOrderAddress MapAddress(ICart cart, cXMLAddress address, string addressId)
        {
            var orderGroupFactory = ServiceLocator.Current.GetInstance<IOrderGroupFactory>();
            var orderAddress = orderGroupFactory.CreateOrderAddress(cart);
            orderAddress.Id = addressId;
            orderAddress.Organization = address.Name;
            orderAddress.Line1 = address.PostalAddress.DeliverTo;
            orderAddress.Line2 = address.PostalAddress.Street;
            orderAddress.PostalCode = address.PostalAddress.PostalCode;
            orderAddress.City = address.PostalAddress.City;
            orderAddress.CountryName = address.PostalAddress.Country.Text;
            orderAddress.CountryCode = address.PostalAddress.Country.IsoCountryCode;
            return orderAddress;
        }
        private void SetBillingAddress(ICart cart, cXMLAddress billTo)
        {
            var billingAddress = MapAddress(cart, billTo, "Billing");
            cart.GetFirstForm().Payments.First().BillingAddress = billingAddress;
        }
        private void SetShippingAddress(ICart cart, cXMLAddress shipTo)
        {
            if (shipTo == null)
            {
                cart.GetFirstShipment().ShippingAddress = cart.GetFirstForm().Payments.First().BillingAddress;
                return;
            }
            var address = MapAddress(cart, shipTo, "Shipping");
            cart.GetFirstShipment().ShippingAddress = address;
        }
        public IReturnData PlaceOrder(CXmlRequestDocument request)
        {
            var session = _punchoutSessionService.GetSettings();
            var cart = _cartService.LoadOrCreateCart(_cartService.DefaultCartName);
            var orderRequest = request.Request.OrderRequest;
            SetCartProperties(cart, request);
            AddCartItems(cart, orderRequest.ItemOut);
            cart.AddPayment(new InvoicePayment());
            SetBillingAddress(cart, orderRequest.OrderRequestHeader.BillTo.Address);
            SetShippingAddress(cart, orderRequest.OrderRequestHeader.ShipTo?.Address);

            _cartService.ValidateCart(cart);
            var orderReference = _orderRepository.SaveAsPurchaseOrder(cart);
            var purchaseOrder = _orderRepository.Load<IPurchaseOrder>(orderReference.OrderGroupId);
            _orderRepository.Delete(cart.OrderLink);

            // Place order
            return new OrderReturnData()
            {
                Success = true
            };
        }

        public IReturnData UpdateOrder(CXmlRequestDocument request)
        {
            throw new NotImplementedException();
        }

        public IReturnData CancelOrder(CXmlRequestDocument request)
        {
            throw new NotImplementedException();
        }
    }

    public class OrderReturnData : IReturnData
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<IItemData> LineItems { get; set; }
        public IReadOnlyDictionary<string, string> CustomOrderData { get; set; }
        public IOrderData OrderData { get; set; }
        public bool Success { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}