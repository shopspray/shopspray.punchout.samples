﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shopspray.PunchOut.Core.Business;
using Shopspray.PunchOut.Core.External;
using System.Web.Security;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Order;
using EPiServer.Logging;
using Microsoft.AspNet.Identity.Owin;
using EPiServer.Web.Routing;
using Shopspray.PunchOut.Core.Enums;
using Mediachase.Commerce.Catalog;
using Microsoft.Owin.Security;

namespace Shopspray.Punchout.Samples.Implementations
{
    public class PunchoutServiceHandler : IPunchoutServiceHandler
    {
        private readonly UserService _userService;
        private readonly ApplicationSignInManager _signInManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly PunchoutSessionService _punchoutSessionService;

        private readonly UrlResolver _urlResolver;
        private readonly ReferenceConverter _referenceConverter;
        private readonly IContentLoader _contentLoader;

        private readonly ICartService _cartService;
        private readonly IOrderRepository _orderRepository;

        private static readonly ILogger Logger = LogManager.GetLogger();

        public PunchoutServiceHandler(
            UserService userService, 
            ApplicationSignInManager signInManager, 
            IAuthenticationManager authenticationManager,
            PunchoutSessionService punchoutSessionService,
            UrlResolver urlResolver,
            IContentLoader contentLoader,
            ReferenceConverter referenceConverter,
            ICartService cartService,
            IOrderRepository orderRepository
            )
        {
            _userService = userService;
            _signInManager = signInManager;
            _authenticationManager = authenticationManager;
            _punchoutSessionService = punchoutSessionService;
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _referenceConverter = referenceConverter;
            _cartService = cartService;
            _orderRepository = orderRepository;
        }

        public bool MayLogin(string username, string password)
        {
            var user = _userService.GetUser(username, password);

            if (user != null && !user.LockoutEnabled)
            {
                return true;
            }

            return false;
        }

        public bool MayLogin(string username)
        {
            var user = _userService.GetUserByName(username);
            return user != null && !user.LockoutEnabled;
        }

        public bool Login(string username)
        {
            if (!MayLogin(username))
            {
                return false;
            }

            var user = _userService.GetUserByName(username);

            if (user != null && !user.LockoutEnabled)
            {
                _signInManager.SignIn(user, true, true);
                return true;
            }

            return false;
        }

        public void Logout()
        {
            EndSession();
            _authenticationManager.SignOut();
        }

        public ISessionContext GetSession()
        {
            var poData = HttpContext.Current.Session["PunchOutData"] as ISessionContext;
            return poData;
        }

        public void SetSession(Guid sessionId)
        {
            if (sessionId != Guid.Empty)
            {

                var context = new SessionContext()
                {
                    IsPunchOut = true,
                    PunchOutSessionId = sessionId
                };

                _punchoutSessionService.StartSession(context);
            }
        }

        public void EndSession()
        {
            _punchoutSessionService.EndSession();
        }

        public string GetProductUrl(string productId)
        {
            try
            {
                var reference = _referenceConverter.GetContentLink(productId);
                var content = _contentLoader.Get<EntryContentBase>(reference);

                if (content is VariationContent)
                {
                    var variationContent = (VariationContent)content;
                    var parent = variationContent.GetParentProducts().FirstOrDefault();
                    return _urlResolver.GetUrl(parent);
                }

                return _urlResolver.GetUrl(content.ContentLink);
            }
            catch (Exception){}

            return "/";
        }

        public bool IsSessionExpired(DateTime created)
        {
            return DateTime.UtcNow.AddHours(-1d) > created.ToUniversalTime();
        }

        public List<string> Search(string searchString)
        {
            return new List<string>() { "123", "456", "789" };
        }

        public string GetSearchPageUrl(string searchString, string vendor)
        {
            return string.Format("search/?q={0}", searchString);
        }

        ISessionContext IPunchoutServiceHandler.GetSession()
        {
            return _punchoutSessionService.GetSettings();
        }

        public void AddToCart(List<KeyValuePair<string, decimal>> items)
        {
            if (items != null && items.Count > 0)
            {
                //We need to force the cart by name and not by session as the session cart might not be loaded correct yet.
                var warning = "";
                var cart = _cartService.LoadOrCreateCart(_cartService.DefaultCartName);
                var cartItems = cart.GetAllLineItems();
                //If there already are items, the session has already been used and we dont want to add items again.
                if (cartItems == null || !cartItems.Any())
                {
                    foreach (var item in items)
                    {
                        _cartService.AddToCart(cart, item.Key, out warning, item.Value);
                        _orderRepository.Save(cart);
                    }
                }
            }
        }

        public void AppendLogMessage(string message, LogLevelType logLevel, string context)
        {
            Logger.Log(GetLevel(logLevel), message);
        }

        private Level GetLevel(LogLevelType level)
        {
            switch (level)
            {
                case LogLevelType.Info:
                    return Level.Information;
                case LogLevelType.Warning:
                    return Level.Warning;
                case LogLevelType.Error:
                    return Level.Error;
                case LogLevelType.Debug:
                    return Level.Debug;
            }

            return Level.Debug;
        }
    }
}