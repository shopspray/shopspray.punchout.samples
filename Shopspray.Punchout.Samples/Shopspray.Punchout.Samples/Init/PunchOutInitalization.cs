﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Shopspray.PunchOut.Core.Business;
using Shopspray.PunchOut.Core.External;
using Shopspray.PunchOut.Customer;
using Shopspray.PunchOut.Initialization;


namespace Shopspray.Punchout.Samples.Init
{
    [InitializableModule]
    [ModuleDependency(typeof(DependenciesModule))]
    public class PunchOutInitalization : IConfigurableModule
    {
        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public void Preload(string[] parameters)
        {
        }

        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Container.Configure(_ =>
            {
                _.For<IPunchoutServiceHandler>().Use<PunchoutServiceHandler>();
                _.For<IUserContextService>().Use<UserContextService>();
                _.For<IOrderContextService>().Use<OrderContextService>();
                _.For<IOrganizationService>().Use<OrganizationService>();
                _.For<PunchoutSessionService>().Use<PunchoutSessionService>();
                _.For<SessionContextService>().Use<SessionContextService>();
            });
        }
    }
}